#include <stdio.h>

#define BUF_SIZE 256

int main(int argc, char *argv[]) {
    char inputted_string[BUF_SIZE];
    scanf("%s", inputted_string);
    printf("Hello, %s!\n", inputted_string);
    return 0;
}