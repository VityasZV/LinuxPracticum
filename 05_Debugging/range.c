#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	if (argc < 2) {
		printf("usage: %s d [d] [d]\n", argv[0]);
		return 1;
	}
	if(argc == 2) {
		int N = atoi(argv[1]);
		int i = 0;
		for(i = 0; i < N; i++) {
			printf("%d\n", i);
		}
	}
	if(argc == 3) {
		int N = atoi(argv[1]);
		int M = atoi(argv[2]);
		int i = 0;
		for(i = N; i < M; i++) {
			printf("%d\n", i);
		}
	}
	if(argc == 4) {
		int N = atoi(argv[1]);
		int M = atoi(argv[2]);
		int S = atoi(argv[3]);
		int i = 0;
		for(i = N; i < M; i+=S) {
			printf("%d\n", i);
		}
	}
	return 0;
}
